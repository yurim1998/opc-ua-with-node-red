# OPC UA with Node-RED, MQTT, FreeRTOS and Elipse E3

---

## Purpose of this project

This project was elaborated for didatic purposes during the undergraduate course of Mechatronics Engineering. For development, concepts such as ESP32, Elipse, MQTT, OPC UA, FreeRTOS and Node-RED were used.

---

## Requirements:

### Node-RED
Install [Node-RED][1]

### node-red-contrib-iiot-opcua
Install [node-red-contrib-iiot-opcua][2]


Run command on Node-RED directory:

	npm install node-red-contrib-iiot-opcua


Alternatively, you may use the command for global installation:

	npm install -g node-red-contrib-iiot-opcua

### Aedes MQTT Broker
Install [Aedes][3] to use it as MQTT Broker:

	npm install aedes

### Elipse E3 Studio
Install [Elipse E3 Studio][4] (32-bit recommended) with Demo.

### ESP32 Arduino core for FreeRTOS
Get FreeRTOS installing [Arduino core for the ESP32][5] (installation instructions in the repository page description).

### TagoIO
Create an account in [tago.io][6].

### Clone this repository:
	
	git clone https://yurim1998@bitbucket.org/yurim1998/opc-ua-with-node-red.git

---

## Recommendation

We recommend using [PlatformIO][7], as this repository uses it to program ESP32 and already has the workspace prepared.

[1]:https://github.com/node-red/node-red
[2]:https://github.com/cacamille3/node-red-contrib-iiot-opcua
[3]:https://github.com/moscajs/aedes
[4]:https://www.elipse.com.br/en/downloads/?cat=19&key=&language=enus
[5]:https://github.com/espressif/arduino-esp32
[6]:https://tago.io/
[7]:https://platformio.org/