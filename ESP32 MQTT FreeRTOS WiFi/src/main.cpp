#include <Arduino.h>
#include <WiFi.h>
#include <PubSubClient.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

// Pin defines
#define LED1 23
#define LED2 22
#define POTENTIOMETER 34

// WiFi ssid, password
const char* ssid = "My WiFi SSID";
const char* password = "My WiFi password";

// MQTT user, password
const char* MQTT_passwd = "mymqttpasword";
const char* MQTT_user = "mymqttuser";

// MQTT
WiFiClient client;
PubSubClient MQTT(client);

//
// FreeRTOS Tasks
//

// WiFi connect task
void Task_wifiConnect(void * param){
  // Task parameter
  (void) param;
  
  // Task infinite loop
  while(1)
  {
    if(WiFi.status() != WL_CONNECTED)
    {
      Serial.print("Connecting to ");
      Serial.println(ssid);

      // Begin connection
      WiFi.begin(ssid, password);

      // While not connected, wait
      while (WiFi.status() != WL_CONNECTED) 
      {

        // FreeRTOS delay
        vTaskDelay(500/portTICK_PERIOD_MS);
        
        Serial.print(".");
      }

      // Print local IP address and confirms connections
      Serial.println("");
      Serial.println("WiFi connected.");
      Serial.println("IP address: ");
      Serial.println(WiFi.localIP());
    }

    // FreeRTOS delay
    vTaskDelay(1000/portTICK_PERIOD_MS);
  }
}


// MQTT connect and loop task
void Task_MQTT(void * param)
{
  (void) param;
 
  // Task infinite loop
  while(1)
  {

    // MQTT must be disconnected and WiFi connected
    if(!MQTT.connected() && WiFi.status() == WL_CONNECTED)
    {

      // MQTT connection
      if(MQTT.connect("My ESP 32 Name",  MQTT_user, MQTT_passwd))
      {
        Serial.println(F("Connected to MQTT!"));

        // Subscribe to topics
        if(MQTT.subscribe("esp/led1"))
        {
          Serial.println(F("Topic esp/led1 subscribed!"));
        }
        if(MQTT.subscribe("esp/led2"))
        {
          Serial.println(F("Topic esp/led2 subscribed!"));
        }
      }
      else
      {
        Serial.print("MQTT error! ");
        Serial.println(MQTT.state());

        // FreeRTOS delay
        vTaskDelay(1000/portTICK_PERIOD_MS);

        // In case of software failure
        ESP.restart(); 
      }
    }

    // If connected, loop
    if(MQTT.connected())
    {

      // MQTT loop
      MQTT.loop();
    }

    // FreeRTOS delay
    vTaskDelay(10/portTICK_PERIOD_MS);
  }
}


// Analog read and publish task
void Task_Analog_Read(void *param)
{
  (void) param;

  // Task infinite loop
  while(1)
  {
    float tensao = (3.3*analogRead(POTENTIOMETER))/4095;

    // Only publish if connected
    if(MQTT.connected())
    {
      String valor = String(tensao);

      char valor_tensao [4];
      for(int i = 0; i < 4; i++)
      {
        valor_tensao[i] = valor[i];
      }
      Serial.printf("Potentiometer tensios: %.2f\n", valor_tensao);

      // Publish value on "esp/data" topic
      MQTT.publish("esp/data", valor_tensao);
    }

    // FreeRTOS delay
    vTaskDelay(1000/portTICK_PERIOD_MS);
  }
}

// MQTT callback function
void callback(char* topic, byte* message, unsigned int length){
  
  // Optional print
  /*Serial.print("Topic message: ");
  Serial.println(topic);*/

  String messageTemp;

  for (int i = 0; i < length; i++) 
  {
    // Optional (see the message in the serial monitor)
    //Serial.println((char)message[i]); 

    messageTemp += (char)message[i];
  }

  // Control the LEDs according to the message received
  if(String(topic) == "esp/led1")
  {
    if (messageTemp == "1")
      digitalWrite(LED1, HIGH);
    if (messageTemp == "0")
      digitalWrite(LED1, LOW);
  }
  else if(String(topic) == "esp/led2")
  {
    if (messageTemp == "1")
      digitalWrite(LED2, HIGH);
    if (messageTemp == "0")
      digitalWrite(LED2, LOW);
  }
}

void setup() 
{
  // Pins and serial
  pinMode(LED2, OUTPUT);
  pinMode(LED1, OUTPUT);
  pinMode(POTENTIOMETER, INPUT);
  digitalWrite(LED2,LOW);
  digitalWrite(LED1,LOW);
  Serial.begin(9600);

  // Set your broker server here
  MQTT.setServer("192.168.x.x", 1883);
  MQTT.setCallback(callback);

  // Tasks created using FreeRTOS and pinned to ESP32 Arduino Core
  xTaskCreatePinnedToCore(Task_wifiConnect, 
                        "Task WiFi", 
                        5000, 
                        NULL, 
                        4, 
                        NULL,
                        1);
  xTaskCreatePinnedToCore(Task_MQTT, 
                        "Task MQTT", 
                        10000, 
                        NULL, 
                        3, 
                        NULL,
                        1);
  xTaskCreatePinnedToCore(Task_Analog_Read,
              "Task ADC",
              5000,
              NULL,
              2,
              NULL,
              1);
}

void loop() {/*empty loop*/}